package com.example.demo.app;

public class SUT {
    boolean verified;

    public SUT(String s) {

    }

    public SUT() {

    }

    public boolean canReceiveRegularWork() {
        return true;
    }

    public void close() {
    }

    public boolean canReceiveAdditionalWork() {
        return false;
    }

    public String hello() {
        return "Hello";
    }

    public String talk() {
        return "How are you?";
    }

    public String bye() {
        return "Bye";
    }

    public String getSystemName() {
        return "Our system under test";
    }

    public boolean isVerified() {
        return verified;
    }

    public void verify() {
        verified = true;
    }

    public boolean hasJobToRun() {
        return true;
    }

    public void run(Job job) {

    }
}
