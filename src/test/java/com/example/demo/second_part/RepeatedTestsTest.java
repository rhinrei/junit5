package com.example.demo.second_part;

import com.example.demo.app.Calculator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class RepeatedTestsTest {

    @RepeatedTest(value = 5, name =
            "{displayName} - repetition {currentRepetition}/{totalRepetitions}")
    @DisplayName("Test add operation")
    void addNumber() {
        Calculator calculator = new Calculator();
        assertEquals(2, calculator.add(1, 1),
                "1 + 1 should equal 2");
    }

    @RepeatedTest(value = 3, name = "repetition= {currentRepetition}")
    void testIf(RepetitionInfo repetitionInfo) {
        if (repetitionInfo.getCurrentRepetition() == 2) System.out.println("Я - второе повторение");
    }
}