package com.example.demo.first_part.extension;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@ExtendWith(CustomExtension.class)
public class ExtensionTests {

    @Test
    public void myCustomRuleTest() {
        System.out.println("Call of a test method");
    }
}
