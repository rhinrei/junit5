package com.example.demo.first_part.base;

import com.example.demo.app.Calculator;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DemoApplicationTests {

	@Test
	public void testAdd() {
		Calculator calculator = new Calculator();
		double result = calculator.add(20, 50);
		assertEquals(60, result, 0);
	}

}
